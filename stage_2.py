import math


def number_of_months_calc(principal):
    monthly_payment = int(input("""Enter the monthly payment:
    """))
    months = math.ceil(principal / monthly_payment)

    print()
    if months == 1:
        print(f"It takes {months} month to repay the credit")
    else:
        print(f"It takes {months} months to repay the credit")


def monthly_payment_calc(principal):
    months = int(input("""Enter the count of months:
    """))
    monthly_payment = math.ceil(principal / months)
    lastpayment = principal - (months - 1) * monthly_payment

    print()
    if lastpayment != monthly_payment:
        print(f"Your monthly payment = {monthly_payment} with last monthly payment = {lastpayment}.")
    else:
        print(f"Your monthly payment = {monthly_payment}.")


principal = int(input("""Enter the credit principal:
"""))
action = input('''What do you want to calculate?
type "m" - for the number of months,
type "p" - for the monthly payment:
''')

if action == "m":
    number_of_months_calc(principal)
elif action == "p":
    monthly_payment_calc(principal)
