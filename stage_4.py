# python stage_4.py --type=diff --principal=1000000 --periods=10 --interest=10
# python stage_4.py --type=annuity --principal=1000000 --periods=60 --interest=10
# python stage_4.py --type=diff --principal=1000000 --payment=104000
# python stage_4.py --type=diff --principal=500000 --periods=8 --interest=7.8
# python stage_4.py --type=annuity --payment=8722 --periods=120 --interest=5.6
# python stage_4.py --type=annuity --principal=500000 --payment=23000 --interest=7.8

import argparse
import math

parser = argparse.ArgumentParser()
parser.add_argument('--type', type=str)
parser.add_argument('--payment', type=float)
parser.add_argument('--principal', type=float)
parser.add_argument('--periods', type=int)
parser.add_argument('--interest', type=float)

args = parser.parse_args()
type = args.type
payment = args.payment
principal = args.principal
periods = args.periods
interest = args.interest


def differentiated_payment_calc(principal, periods, interest):
    interest_rate = interest / (12 * 100)
    payment_sum = 0
    for month in range(periods):
        payment = math.ceil(principal / periods + interest_rate * (principal - (principal * month) / periods))
        payment_sum += payment
        print(f"Month {month + 1}: paid out {payment}")
    overpayment = int(payment_sum - principal)
    print()
    print(f"Overpayment = {overpayment}")


def annuity_payment_calc(principal, periods, interest):
    interest_rate = interest / (12 * 100)
    payment = math.ceil(principal * interest_rate * (1 + interest_rate) ** periods / (
            (1 + interest_rate) ** periods - 1))

    print(f"Your annuity payment = {payment}!")
    overpayment = int(payment * periods - principal)
    print(f"Overpayment = {overpayment}")


def annuity_periods_calc(payment, principal, interest):
    interest_rate = interest / (12 * 100)
    periods = math.ceil(
        math.log((payment / (payment - interest_rate * principal)), 1 + interest_rate))

    years = periods // 12
    months = periods - years * 12
    years_word = "years"
    if years == 1:
        years_word = "year"
    months_word = "months"
    if months == 1:
        months_word = "month"

    if years == 0:
        print(f"You need {months} {months_word} to repay this credit!")
    else:
        if months == 0:
            print(f"You need {years} {years_word} to repay this credit!")
        else:
            print(f"You need {years} {years_word} and {months} {months_word} to repay this credit!")
    overpayment = int(payment * periods - principal)
    print(f"Overpayment = {overpayment}")


def annuity_principal_calc(payment, periods, interest):
    interest_rate = interest / (12 * 100)
    principal = math.floor(payment / (
            (interest_rate * (1 + interest_rate) ** periods) / ((1 + interest_rate) ** periods - 1)))

    print(f"Your credit principal = {principal}!")
    overpayment = int(payment * periods - principal)
    print(f"Overpayment = {overpayment}")


error_message = "Incorrect parameters"
if payment != None and payment < 0:
    print(error_message)
elif principal != None and principal < 0:
    print(error_message)
elif periods != None and periods < 0:
    print(error_message)
elif interest != None and interest < 0:
    print(error_message)
elif type == "diff":
    if payment == None and principal != None and periods != None and interest != None:
        differentiated_payment_calc(principal, periods, interest)
    else:
        print(error_message)
elif type == "annuity":
    if payment == None and principal != None and periods != None and interest != None:
        annuity_payment_calc(principal, periods, interest)
    elif payment != None and principal != None and periods == None and interest != None:
        annuity_periods_calc(payment, principal, interest)
    elif payment != None and principal == None and periods != None and interest != None:
        annuity_principal_calc(payment, periods, interest)
    else:
        print(error_message)
else:
    print(error_message)
