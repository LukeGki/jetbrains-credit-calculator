import math


def count_of_months_calc():
    credit_principal = float(input("""Enter the credit principal:
    """))
    monthly_payment = float(input("""Enter the annuity payment:
    """))
    credit_interest = float(input("""Enter the credit interest:
    """))

    interest_rate = credit_interest / (12 * 100)
    months = math.ceil(
        math.log((monthly_payment / (monthly_payment - interest_rate * credit_principal)), 1 + interest_rate))

    years = months // 12
    months_with_years = months - years * 12
    years_word = "years"
    if years == 1:
        years_word = "year"
    months_word = "months"
    if months_with_years == 1:
        months_word = "month"

    if years == 0:
        print(f"You need {months_with_years} {months_word} to repay this credit!")
    else:
        if months_with_years == 0:
            print(f"You need {years} {years_word} to repay this credit!")
        else:
            print(f"You need {years} {years_word} and {months_with_years} {months_word} to repay this credit!")


def annuity_monthly_payment():
    credit_principal = float(input("""Enter the credit principal:
    """))
    number_of_periods = int(input("""Enter the number of periods:
    """))
    credit_interest = float(input("""Enter the credit interest:
    """))

    interest_rate = credit_interest / (12 * 100)
    monthly_payment = math.ceil(credit_principal * interest_rate * (1 + interest_rate) ** number_of_periods / (
            (1 + interest_rate) ** number_of_periods - 1))

    print(f"Your annuity payment = {monthly_payment}!")


def credit_principal():
    monthly_payment = float(input("""Enter the annuity payment:
    """))
    number_of_periods = int(input("""Enter the number of periods:
    """))
    credit_interest = float(input("""Enter the credit interest:
    """))

    interest_rate = credit_interest / (12 * 100)
    credit_principal = monthly_payment / (
            (interest_rate * (1 + interest_rate) ** number_of_periods) / ((1 + interest_rate) ** number_of_periods - 1))

    print(f"Your credit principal = {credit_principal}!")


action = input('''What do you want to calculate?
type "n" for the count of months,
type "a" for the annuity monthly payment,
type "p" for the credit principal:
''')

if action == "n":
    count_of_months_calc()
elif action == "a":
    annuity_monthly_payment()
elif action == "p":
    credit_principal()
